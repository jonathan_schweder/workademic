package com.work.entity;

import java.util.List;

class Autor {
    private String nome;
    private List<Artigo> artigos;
    private String email;
    private String senha;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Artigo> getObras() {
        return artigos;
    }

    public void setObras(List<Artigo> artigos) {
        this.artigos = artigos;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
    
    
}

package com.work.entity;

import java.util.List;
import javax.swing.text.html.parser.Entity;

//@Entity teste
public class Artigo {
    private String titulo;
    private String Resumo;
    private List<Area> area;    
    private List<Autor> autor;
    private List<PalavraChave> palavrasChaves;    
    private List<Artigo> artigosRelacionadas;

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getResumo() {
        return Resumo;
    }

    public void setResumo(String Resumo) {
        this.Resumo = Resumo;
    }

    public List<Area> getArea() {
        return area;
    }

    public void setArea(List<Area> area) {
        this.area = area;
    }

    public List<Autor> getAutor() {
        return autor;
    }

    public void setAutor(List<Autor> autor) {
        this.autor = autor;
    }

    public List<PalavraChave> getPalavrasChaves() {
        return palavrasChaves;
    }

    public void setPalavrasChaves(List<PalavraChave> palavrasChaves) {
        this.palavrasChaves = palavrasChaves;
    }

    public List<Artigo> getObrasRelacionadas() {
        return artigosRelacionadas;
    }

    public void setObrasRelacionadas(List<Artigo> artigosRelacionadas) {
        this.artigosRelacionadas = artigosRelacionadas;
    }
 
    
}
